import { DiscoverMovieResponse, DiscoverMovieError } from "./movieTypes";

export async function fetchMovies(
    page = 1,
    genres: number[] = []
): Promise<DiscoverMovieResponse> {
    const genresParam = genres.length ? "&with_genres=" + genres.join(",") : "";

    const response = await fetch(
        `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.REACT_APP_API_KEY}&page=${page}${genresParam}`
    );
    if (response.ok) {
        return response.json();
    }
    const body: DiscoverMovieError = await response.json();
    throw new Error(body.status_message);
}
