import { fetchMovies } from "./api";
import { server } from "../mocks/server";

beforeAll(() => server.listen({ onUnhandledRequest: "error" }));
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("fetchMovies", () => {
    test("given arguments makes the right request", async () => {
        process.env.REACT_APP_API_KEY = "valid_key";
        const movieResponse = await fetchMovies(2, [28, 12]);
        expect(movieResponse.page).toBe(2);
        const { genre_ids } = movieResponse.results[0];
        expect(genre_ids.includes(28)).toBe(true);
        expect(genre_ids.includes(12)).toBe(true);
    });

    test("works with no arguments", async () => {
        process.env.REACT_APP_API_KEY = "valid_key";
        const movieResponse = await fetchMovies();
        expect(movieResponse.page).toBe(1);
    });

    test("when server returns a non 200 response returns a rejected promise", () => {
        process.env.REACT_APP_API_KEY = "invalid_key";
        const onResolved = jest.fn();
        return fetchMovies()
            .then(onResolved, (err) => {
                expect(err.message).toBe(
                    "Invalid API key: You must be granted a valid key."
                );
            })
            .then(() => {
                expect(onResolved).not.toBeCalled();
            });
    });
});
