import useMovies from "./useMovies";
import { renderHook, act } from "@testing-library/react-hooks";
import { server } from "./mocks/server";

beforeAll(() => server.listen({ onUnhandledRequest: "error" }));
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("useMovies", () => {
    test("when calling loadMore appends new movies", async () => {
        process.env.REACT_APP_API_KEY = "valid_key";
        const { result, waitForNextUpdate } = renderHook(() => useMovies());

        expect(result.current.loading).toBe(true);

        await waitForNextUpdate();

        expect(result.current.loading).toBe(false);

        expect(result.current.movies.length).toBe(20);

        const first = result.current.movies;

        act(() => {
            result.current.loadMore();
        });

        expect(result.current.loadingMore).toBe(true);

        await waitForNextUpdate();

        expect(result.current.loadingMore).toBe(false);
        expect(result.current.movies.length).toBe(40);
        const firstAndSecond = result.current.movies;
        expect(firstAndSecond.slice(0, 20)).toEqual(first);

        act(() => {
            result.current.loadMore();
        });

        expect(result.current.loadingMore).toBe(true);

        await waitForNextUpdate();

        expect(result.current.loadingMore).toBe(false);
        expect(result.current.movies.length).toBe(60);
        expect(result.current.movies.slice(0, 40)).toEqual(firstAndSecond);
    });

    test("on error returns an error", async () => {
        process.env.REACT_APP_API_KEY = "invalid_key";

        const { result, waitForNextUpdate } = renderHook(() => useMovies());
        expect(result.current.error).toBe(undefined);

        await waitForNextUpdate();

        expect(result.current.error).toBeTruthy();
    });
});
