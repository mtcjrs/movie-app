import {
    render,
    screen,
    waitForElementToBeRemoved,
} from "@testing-library/react";
import App from "./App";
import { server } from "./mocks/server";
import userEvent from "@testing-library/user-event";

beforeAll(() => server.listen({ onUnhandledRequest: "error" }));
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("load more movies without genre filter, then select comedy and load more again", async () => {
    process.env.REACT_APP_API_KEY = "valid_key";
    render(<App />);

    expect(screen.getByTestId("spinner")).toBeInTheDocument();

    let more = await screen.findByRole("button", { name: /load more/i });

    expect(screen.getAllByTestId("movie-card").length).toBe(20);

    userEvent.click(more);

    await waitForElementToBeRemoved(screen.getByTestId("spinner"));

    expect(screen.getAllByTestId("movie-card").length).toBe(40);

    let comedyCheckbox = screen.getByRole("checkbox", { name: /comedy/i });
    userEvent.click(comedyCheckbox);
    expect(comedyCheckbox).toBeChecked();

    expect(screen.getByTestId("spinner")).toBeInTheDocument();

    expect(screen.queryAllByTestId("movie-card").length).toBe(0);
    await waitForElementToBeRemoved(screen.getByTestId("spinner"));

    more = screen.getByRole("button", { name: /load more/i });

    expect(screen.getAllByTestId("movie-card").length).toBe(20);
    expect(
        screen.getByRole("heading", { name: /unique to comedy page 1/i })
    ).toBeInTheDocument();

    userEvent.click(more);

    await waitForElementToBeRemoved(screen.getByTestId("spinner"));
    expect(screen.getAllByTestId("movie-card").length).toBe(40);
    expect(
        screen.getByRole("heading", { name: /unique to comedy page 1/i })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("heading", { name: /unique to comedy page 2/i })
    ).toBeInTheDocument();
});

test("display an error to the user", async () => {
    process.env.REACT_APP_API_KEY = "invalid_key";
    render(<App />);

    expect(screen.getByTestId("spinner")).toBeInTheDocument();
    await waitForElementToBeRemoved(screen.getByTestId("spinner"));
    screen.getByText(/an error has occurred/i);
});
