import style from "./ShowError.module.css";

interface ShowErrorProps {
    error: Error;
}

export default function ShowError({ error }: ShowErrorProps) {
    return (
        <div className={style.container}>
            <p>
                An error has occurred:
                <br /> {error.message}
            </p>
        </div>
    );
}
