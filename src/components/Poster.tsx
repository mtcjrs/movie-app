import c from "../util/joinClasses";
import picture from "../assets/picture.svg";
import style from "./Poster.module.css";

interface PosterProps {
    className?: string;
    filename: string | null | undefined;
}

export default function Poster({ className, filename }: PosterProps) {
    if (!filename) {
        return (
            <div className={c([style.noPoster, className])}>
                <img className={style.noPosterImage} src={picture} alt="" />
            </div>
        );
    }
    const small = `https://image.tmdb.org/t/p/w220_and_h330_face${filename}`;
    const big = `https://image.tmdb.org/t/p/w440_and_h660_face/${filename}`;
    return (
        <img
            className={c([style.image, className])}
            loading="lazy"
            height="267"
            src={small}
            srcSet={`${small} 1x, ${big} 2x`}
            alt=""
        />
    );
}
