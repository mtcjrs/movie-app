import c from "../util/joinClasses";
import style from "./Score.module.css";

interface ScoreProps {
    className?: string;
    rating: number;
}

export default function Score({ className, rating }: ScoreProps) {
    const percent = rating * 10;
    const normalized = rating / 10;
    const angle = normalized * 2 * Math.PI;
    const longArc = angle > Math.PI / 2 ? 1 : 0;
    const x = Math.round(17 + 19 * Math.sin(angle));
    const y = Math.round(17 - 19 * Math.cos(angle));
    const red = Math.min(230, Math.round(460 - 460 * normalized));
    const green = Math.min(230, Math.round(460 * normalized));

    return (
        <div className={c([style.score, className])}>
            {percent}
            <span className={style.percentSign}>%</span>
            <div className={style.middleRing} />
            <div
                className={style.topRing}
                style={{
                    clipPath: `path('M 17 17 L 17 -2 A 19,19 0,${longArc},1 ${x},${y} L 17 17 z')`,
                    border: `2px solid rgb(${red}, ${green}, 100)`,
                }}
            />
        </div>
    );
}
