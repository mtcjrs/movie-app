import genres from "../api/genres";
import style from "./Genres.module.css";
import c from "../util/joinClasses";

interface GenresProps {
    disabled: boolean;
    setSelected: (genres: number[]) => void;
    selected: number[];
}

export default function Genres({
    disabled,
    selected,
    setSelected,
}: GenresProps) {
    const toggleId = (id: number, isSelected: boolean) => {
        if (disabled) {
            return;
        }
        if (isSelected) {
            setSelected(selected.filter((selectedId) => selectedId !== id));
        } else {
            setSelected([...selected, id]);
        }
    };
    return (
        <ul className={style.list}>
            {genres.map((genre) => {
                const isSelected = selected.includes(genre.id);
                return (
                    <li
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={0}
                        className={c([
                            style.genre,
                            isSelected && style.genreSelected,
                        ])}
                        key={genre.id}
                        onClick={() => {
                            toggleId(genre.id, isSelected);
                        }}
                        onKeyDown={(event) => {
                            if (event.code === "Space") {
                                event.preventDefault();
                                toggleId(genre.id, isSelected);
                            }
                        }}
                    >
                        {genre.name}
                    </li>
                );
            })}
        </ul>
    );
}
