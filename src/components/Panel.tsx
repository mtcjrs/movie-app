import { useState, ReactNode } from "react";
import arrow from "../assets/chevron-right.svg";
import c from "../util/joinClasses";
import style from "./Panel.module.css";

export default function Panel({
    children,
    className,
    panelId,
    title,
}: {
    children: ReactNode;
    className?: string;
    panelId: string;
    title: string;
}) {
    const [showContent, setShowContent] = useState(true);
    const accordionId = `accordion-${panelId}`;
    const sectId = `sect-${panelId}`;

    return (
        <div className={c([style.panel, className])}>
            <h2 className={style.heading}>
                <button
                    aria-controls={sectId}
                    aria-expanded={showContent}
                    className={style.button}
                    id={accordionId}
                    onClick={() => setShowContent(!showContent)}
                >
                    <span>{title}</span>
                    <img
                        alt=""
                        className={showContent ? style.arrow : ""}
                        src={arrow}
                        height="16"
                        width="16"
                    />
                </button>
            </h2>
            <div
                className={c([
                    style.content,
                    !showContent && style.contentHidden,
                ])}
                aria-labelledby={accordionId}
                id={sectId}
                role="region"
            >
                {children}
            </div>
        </div>
    );
}
