import { useEffect, useState } from "react";
import c from "../util/joinClasses";
import logo from "../assets/logo.svg";
import mobileLogo from "../assets/logo-mobile.svg";
import style from "./Header.module.css";
type Direction = "up" | "down";

export default function Header() {
    const [hide, setHide] = useState(false);
    useEffect(() => {
        let prevScroll = window.scrollY;
        let lastDirectionChange = window.scrollY;
        let prevDirection: Direction = "up";
        const checkScroll = () => {
            let scroll = window.scrollY;
            let direction: Direction = "down";
            if (scroll > prevScroll) {
                direction = "down";
            }
            if (scroll < prevScroll) {
                direction = "up";
            }
            if (direction !== prevDirection) {
                prevDirection = direction;
                lastDirectionChange = scroll;
            }
            if (direction === "down" && scroll - lastDirectionChange > 64) {
                setHide(true);
            }
            if (direction === "up" && lastDirectionChange - scroll > 10) {
                setHide(false);
            }
            prevScroll = scroll;
        };
        window.addEventListener("scroll", checkScroll);
        return () => window.removeEventListener("scroll", checkScroll);
    }, []);

    return (
        <header className={c([style.header, hide && style.headerUp])}>
            <div className={style.content}>
                <picture>
                    <source media="(max-width: 767px)" srcSet={mobileLogo} />
                    <img
                        className={style.logo}
                        alt="The Movie Database"
                        src={logo}
                        width="154"
                        height="20"
                    />
                </picture>
            </div>
        </header>
    );
}
