import style from "./Loading.module.css";
import c from "../util/joinClasses";

export default function Loading({ className }: { className?: string }) {
    return (
        <div data-testid="spinner" className={c([style.container, className])}>
            <div className={style.spinner} />
        </div>
    );
}
