import { Movie } from "../api/movieTypes";
import c from "../util/joinClasses";
import Poster from "./Poster";
import Score from "./Score";
import style from "./MovieCard.module.css";

export default function MovieCard({
    className,
    movie,
}: {
    className?: string;
    movie: Movie;
}) {
    return (
        <div className={c([style.movie, className])} data-testid="movie-card">
            <Poster filename={movie.poster_path} className={style.poster} />
            <div className={style.content}>
                <Score rating={movie.vote_average} className={style.score} />
                <h2 className={style.title}>{movie.title}</h2>
                {movie.release_date !== undefined ? (
                    <p className={style.release}>
                        {new Date(movie.release_date).toLocaleDateString(
                            "en-US",
                            {
                                dateStyle: "long",
                            }
                        )}
                    </p>
                ) : null}
                <div className={style.overview}>
                    <p className={style.overviewText}>{movie.overview}</p>
                </div>
            </div>
        </div>
    );
}
