import { Movie } from "../api/movieTypes";
import c from "../util/joinClasses";
import MovieCard from "./MovieCard";
import style from "./Movies.module.css";

interface MoviesProps {
    className?: string;
    movies: Movie[];
}

export default function Movies({ className, movies }: MoviesProps) {
    return (
        <div className={c([style.movies, className])}>
            {movies.map((movie) => (
                <MovieCard movie={movie} key={movie.id} />
            ))}
        </div>
    );
}
