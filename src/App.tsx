import { useState } from "react";
import Genres from "./components/Genres";
import Header from "./components/Header";
import Loading from "./components/Loading";
import Movies from "./components/Movies";
import Panel from "./components/Panel";
import ShowError from "./components/ShowError";
import style from "./App.module.css";
import useMovies from "./useMovies";

function App() {
    const [selected, setSelected] = useState<number[]>([]);
    const { movies, loading, loadingMore, error, loadMore } =
        useMovies(selected);

    const showMovies: boolean = !loading && !error && movies.length !== 0;
    const showLoadMore: boolean = !loading && !error && !loadingMore;
    const showSpinner: boolean = loading || loadingMore;

    return (
        <div className="App">
            <Header />
            <main className={style.main}>
                <Panel
                    title="Filters"
                    panelId="filters"
                    className={style.panel}
                >
                    <h3 className={style.genresHeading}>Genres</h3>
                    <Genres
                        selected={selected}
                        setSelected={setSelected}
                        disabled={loading || loadingMore}
                    />
                </Panel>
                {error && <ShowError error={error} />}
                {showMovies && <Movies movies={movies} />}
                {showLoadMore && (
                    <button
                        className={style.moreButton}
                        onClick={() => loadMore()}
                    >
                        Load More
                    </button>
                )}
                {showSpinner && <Loading className={style.loading} />}
            </main>
        </div>
    );
}

export default App;
