import { useEffect, useState } from "react";
import { fetchMovies } from "./api/api";
import { DiscoverMovieResponse, Movie } from "./api/movieTypes";

interface State {
    response?: DiscoverMovieResponse;
    error?: Error;
    movies: Movie[];
    loading: boolean;
    loadingMore: boolean;
}

export default function useMovies(genres?: number[]) {
    const [state, setState] = useState<State>({
        movies: [],
        loading: false,
        loadingMore: false,
    });

    useEffect(() => {
        setState((state) => ({ ...state, loading: true }));
        fetchMovies(1, genres)
            .then((response) => {
                setState({
                    response,
                    movies: response.results,
                    loading: false,
                    loadingMore: false,
                });
            })
            .catch((error) =>
                setState((state) => ({ ...state, loading: false, error }))
            );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [JSON.stringify(genres)]);

    const loadMore = async () => {
        if (!state.response || state.loading || state.loadingMore) {
            return;
        }

        if (state.response.page === state.response.total_pages) {
            return;
        }

        setState((state) => ({ ...state, loadingMore: true }));
        try {
            const response = await fetchMovies(state.response.page + 1, genres);
            setState((state) => ({
                response,
                movies: [...state.movies, ...response.results],
                loading: false,
                loadingMore: false,
            }));
        } catch (error: any) {
            setState((state) => ({ ...state, loadingMore: false, error }));
        }
    };

    return {
        loading: state.loading,
        loadingMore: state.loadingMore,
        movies: state.movies,
        error: state.error,
        loadMore,
    };
}
