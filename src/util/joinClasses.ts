export default function joinClasses(
    classes: (string | boolean | null | undefined)[]
): string {
    return classes.filter((name) => typeof name === "string").join(" ");
}
