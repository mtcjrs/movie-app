import { rest } from "msw";
import page1 from "./__responses__/page1.json";
import page2 from "./__responses__/page2.json";
import page3 from "./__responses__/page3.json";
import comedy1 from "./__responses__/comedy1.json";
import comedy2 from "./__responses__/comedy2.json";

export const handlers = [
    rest.get("https://api.themoviedb.org/3/discover/movie", (req, res, ctx) => {
        const key = req.url.searchParams.get("api_key");

        if (key !== "valid_key") {
            return res(
                ctx.status(401),
                ctx.json({
                    status_code: 7,
                    status_message:
                        "Invalid API key: You must be granted a valid key.",
                    success: false,
                })
            );
        }
        const genre_ids = req.url.searchParams
            .get("with_genres")
            ?.split(",")
            .map(Number);
        let page = Number(req.url.searchParams.get("page"));
        if (!page) {
            page = 1;
        }
        if (genre_ids?.includes(35)) {
            if (page === 1) {
                return res(ctx.json(comedy1));
            }
            if (page === 2) {
                return res(ctx.json(comedy2));
            }
        }
        let response = page === 1 ? page1 : page === 2 ? page2 : page3;
        response = { ...response };
        response.total_pages = 3;
        if (genre_ids?.length) {
            response.results = response.results.map((result) => ({
                ...result,
                genre_ids: Array.from(
                    new Set([...result.genre_ids, ...genre_ids])
                ),
            }));
        }
        return res(ctx.json(response));
    }),
];
